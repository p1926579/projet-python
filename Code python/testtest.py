# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 01:31:43 2024

@author: moad
"""


import pandas as pd
import matplotlib.pyplot as plt
import os

# Afficher le répertoire de travail
print("Répertoire de travail actuel :", os.getcwd())

# Afficher la liste des fichiers dans le répertoire courant
print("Contenu du répertoire :", os.listdir())

# Chemin complet du fichier m1.csv
m1_csv_path = os.path.join("Data", "m1.csv")
print("Chemin complet de m1.csv :", os.path.abspath(m1_csv_path))

df = pd.read_csv("/builds/p1926579/projet-python/Data/m1.csv",sep=';')

plt.figure(figsize=(8, 6)) 
plt.bar(df['name'],df['ED'], color='skyblue') 
plt.xlabel("Noms") 
plt.ylabel("Moyennes")
plt.title("Moyennes par Nom")
plt.xticks(rotation=90) 
plt.tight_layout() 
plt.savefig("/builds/p1926579/projet-python/Graphes/MoyenneTempCommune.pdf")
plt.show()